* `git clone https://:@gitlab.cern.ch:8443/wittgen/CoolVariantExample.git --recurse-submodules`
* `cd CoolVariantExample`
* `source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-03-00`
* `cmake_config $CMTCONFIG`
* `cd $CMTCONFIG`
* `make -j8 install`
* `cd ..`
* `source installed/setup.sh`
* `writeCoolVariant -d 'sqlite://;schema=./mydb.db;dbname=TESTDB' -f /myfolder -t mytag -j test_data/test.json`
* `readCoolVariant -d 'sqlite://;schema=./mydb.db;dbname=TESTDB' -f /myfolder -t mytag`
